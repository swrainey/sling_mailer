# README

## Installation

`bundle install`

`rake db:migrate`

## Tests

`rspec spec`



## Running

Copy `.env.test` to `.env` then add your own email service provider details where applicable.

`bundle exec rackup`


#### POST http://127.0.0.1:9292/email


Required Params:

`to, to_name, from, from_name, subject, body`


#### GET http://127.0.0.1:9292/status

Returns status 200 for use with monitoring service




## Assumptions
This system will be used for low volume transactional emails that need to be delivered in a timely manner and therefore have increased resiliency from a third party outage.

## Platform Decisions

### Sinatra

Ruby was chosen because the existing projects have been mostly written in Ruby or Javascript. Sinatra was chosen because this service really only needed the use of a very lightweight framework. Sinatra is one of the more popular  choices for this.

### Postgres/Sequel
Postgres was chosen because there was a future desire to have the email history available via a query format. Sequel was chosen because it's a lightweight ORM that gave us a few things we could make use of for the project, Specifically standardized data migrations and validations. This will set the project up to be iterated on with other developers.

Sequel was a choice that I was more on the fence about because of the sheer simplicity of the project.


### Html2Text gem
Included the Html2Text gem to assist with converting the html mail body to text while giving the best attempt and preserving line breaks.


## Future Enhancements

### Delay email sends
If this system has the potential for spikes or higher volume a queuing system and workers should be added.


### Security
The system is not using any sort of authorization. It is assumed that it is protected by the network setup in production. An appropriate authorization mechanism should be decided upon.


### Logging & Error handling
Appropriate logging would be set up and tied in to a production monitoring system to monitor if the system is falling back to the secondary provider or failing.

Specifically the schedule method in the email providers could care for and handle different types of errors better.

### Testing
Testing needs to be further fleshed out.
