# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:emails) do
      primary_key :id
      String :to
      String :to_name
      String :from
      String :from_name
      String :subject
      String :body, text: true
      DateTime :created_at
      String :service_provider
      String :status
      String :messageid
    end
  end
end
