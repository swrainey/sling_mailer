# frozen_string_literal: true

require 'net/https'
require 'html2text'

EMAIL_REGEXP = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i.freeze

# Base email sequel email class
class Email < Sequel::Model
  plugin :validation_helpers
  plugin :json_serializer

  def validate
    super
    validates_presence %i[to to_name from from_name subject body],
                       message: 'is required'
    validates_format EMAIL_REGEXP, %i[to from],
                     message: 'must be a valid email address'
  end

  def json_response
    to_json(only: %i[id messageid service_provider])
  end

  def body_text
    Html2Text.convert(body)
  end
end
