# frozen_string_literal: true

# Handles scheduling emails directly with Postmark
class Postmark < Email
  def schedule
    response = api_request
    self.service_provider = 'postmark'
    begin
      return false unless response['ErrorCode'].zero?

      self.messageid = response['MessageID']
      save
      true
    rescue StandardError
      false
    end
  end

  def api_request
    http = Net::HTTP.new('api.postmarkapp.com', 443)
    http.use_ssl = true
    request = Net::HTTP::Post.new('/email')
    request['X-Postmark-Server-Token'] = ENV['POSTMARK_SERVER_API_TOKEN']
    request['Accept'] = 'application/json'
    request['Content-Type'] = 'application/json'
    request.body = request_body

    response = http.request(request)
    JSON.parse(response.body)
  end

  def request_body
    body_json = {}
    body_json[:from] = "#{from_name} <#{from}>"
    body_json[:to] = "#{to_name} <#{to}>"
    body_json[:subject] = subject
    body_json[:htmlbody] = body
    body_json[:textbody] = body_text
    body_json[:replyto] = from

    body_json.to_json
  end
end
