# frozen_string_literal: true

# Handles scheduling emails directly with Sendgrid
class Sendgrid < Email
  def schedule
    response = api_request

    self.service_provider = 'sendgrid'
    begin
      raise StandardError unless response.code == '202'

      self.messageid = response['X-Message-Id']
      save
      true
    rescue StandardError
      false
    end
  end

  def api_request
    http = Net::HTTP.new('api.sendgrid.com', 443)
    http.use_ssl = true
    request = Net::HTTP::Post.new('/v3/mail/send')
    request['Authorization'] = 'Bearer ' + ENV['SENDGRID_API_KEY']
    request['Content-Type'] = 'application/json'
    request.body = request_body
    http.request(request)
  end

  def request_body
    request_body = {}
    request_body[:personalizations] = [{ to: [{ email: to, name: to_name }],
                                         subject: subject }]

    request_body[:from] = { email: from, 'name': from_name }
    request_body[:content] = [{ type: 'text/plain', value: body_text },
                              { type: 'text/html', value: body }]

    request_body.to_json
  end
end
