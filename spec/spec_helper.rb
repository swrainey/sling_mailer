# frozen_string_literal: true

require 'rack/test'
require 'rspec'
require 'webmock/rspec'

require 'dotenv'
Dotenv.load('.env.test')

ENV['RACK_ENV'] = 'test'

WebMock.disable_net_connect!(allow_localhost: true)

module RSpecMixin
  include Rack::Test::Methods
  def app
    described_class
  end
end

module StubExternalRequest
  def sendgrid_request
    stub_request(:post, 'https://api.sendgrid.com/v3/mail/send')
      .with(
        body: Sendgrid.new(@mail_params).request_body,
        headers: {
          'Authorization' => 'Bearer sendgridtoken',
          'Content-Type' => 'application/json'
        }).to_return(status: 202, body: '', headers: { 'X-Message-Id': 'MSGID' })
  end

  def bad_sendgrid_request
    stub_request(:post, 'https://api.sendgrid.com/v3/mail/send')
      .with(
        body: Sendgrid.new(@mail_params).request_body,
        headers: {
          'Authorization' => 'Bearer sendgridtoken',
          'Content-Type' => 'application/json'
        }).to_return(status: 500, body: '', headers: {})
  end

  def postmark_request
    stub_request(:post, 'https://api.postmarkapp.com/email')
      .with(
        body: Postmark.new(@mail_params).request_body,
        headers: {
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
          'X-Postmark-Server-Token' => 'postmarktoken'
        }).to_return(status: 200, body: '{"ErrorCode": 0, "Message": "OK",
                                         "MessageID": "MSGID"}', headers: {})
  end

  def bad_postmark_request
    stub_request(:post, 'https://api.postmarkapp.com/email')
      .with(
        body: Postmark.new(@mail_params).request_body,
        headers: {
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
          'X-Postmark-Server-Token' => 'postmarktoken'
        }).to_return(status: 500, body: '{}', headers: {})
  end
end

RSpec.configure do |config|
  config.include RSpecMixin
  config.include StubExternalRequest

  config.before(:each) do
    @mail_params = { to: 'recipient@example.com', to_name: 'Test Recipient',
                     from: 'sender@example.com', from_name: 'Test Sender',
                     subject: 'Test Subject',
                     body: '<h1>Hello World</h1><p>Testing, Testing.</p>' }
  end
end

require_relative '../db/connect'
Sequel.extension :migration

Sequel::Migrator.run(DB, 'db/migrations', target: 0)
Sequel::Migrator.run(DB, 'db/migrations')

require File.join(File.dirname(__FILE__), '../app')
