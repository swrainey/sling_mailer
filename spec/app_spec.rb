# frozen_string_literal: true

require_relative 'spec_helper'

def app
  ApplicationController
end

describe ApplicationController do
  describe '#email' do
    context 'when missing parameters' do
      it 'responds with 422 when missing parameters' do
        test_params = @mail_params.dup
        test_params['to'] = ''
        post '/email', test_params
        expect(last_response.status).to eq(422)
      end
    end

    context 'when primary provider is successful' do
      before { sendgrid_request }
      before { postmark_request }

      it 'responds with 200 when all parameters are passed' do
        post '/email', @mail_params
        expect(last_response.status).to eq(200)
      end

      it 'successfully sends to sendrid as primary provider' do
        post '/email', @mail_params
        response_json = JSON.parse(last_response.body)
        expect(response_json['service_provider']).to eq('sendgrid')
      end
    end

    context 'when primary provider errors' do
      before { bad_sendgrid_request }
      before { postmark_request }

      it 'fallsback to postmark if sendgrid errors' do
        post '/email', @mail_params
        response_json = JSON.parse(last_response.body)
        expect(response_json['service_provider']).to eq('postmark')
      end
    end

    context 'when both providers are down' do
      before { bad_sendgrid_request }
      before { bad_postmark_request }

      it 'returns 500' do
        post '/email', @mail_params
        expect(last_response.status).to eq(500)
      end
    end
  end

  describe '#status' do
    it 'responds with 200 when all parameters are passed' do
      get '/status'
      expect(last_response.status).to eq(200)
    end
  end
end
