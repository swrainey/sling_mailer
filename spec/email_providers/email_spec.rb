# frozen_string_literal: true

describe Email do
  describe '#validate' do
    context 'when missing or invalid attributes' do
      let(:record) { Email.new }

      it 'validates presence' do
        record.valid? # run validations
        %i[to to_name from from_name body subject].each do |attr|
          expect(record.errors).to include(attr)
        end
        expect(record.errors[:to]).to include('must be a valid email address')
      end

      it 'validates to email format' do
        record.to = 'example.com'
        record.valid? # run validations
        expect(record.errors[:to]).to include('must be a valid email address')

        record.to = 'example@example.com'
        record.valid? # run validations
        expect(record.errors).not_to include(:to)
      end

      it 'validates from email format' do
        record.from = 'example.com'
        record.valid? # run validations
        expect(record.errors[:from]).to include('must be a valid email address')

        record.from = 'example@example.com'
        record.valid? # run validations
        expect(record.errors).not_to include(:from)
      end
    end
  end
end
