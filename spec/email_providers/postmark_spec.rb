# frozen_string_literal: true

require_relative '../spec_helper'

describe Postmark do
  describe '#schedule' do
    let(:record) { Postmark.new(@mail_params) }

    context 'when request succeeds' do
      before { postmark_request }
      it 'returns true' do
        expect(record.schedule).to eq(true)
      end

      it 'saves the record with a message id' do
        record.schedule
        saved_record = Postmark[record.id]
        expect(saved_record.messageid).to eq('MSGID')
      end
    end

    context 'when request fails' do
      before { bad_postmark_request }
      it 'returns false' do
        expect(record.schedule).to eq(false)
      end

      it 'does not save the record' do
        record.schedule
        saved_record = Postmark[record.id]
        expect(saved_record).to eq(nil)
      end
    end
  end
end
