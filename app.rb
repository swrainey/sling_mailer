# frozen_string_literal: true

require 'sinatra'
require 'logger'
require 'dotenv/load'
require_relative 'db/connect'

require_relative 'email_providers/email'

require_relative 'email_providers/sendgrid'
require_relative 'email_providers/postmark'

PRIMARY_PROVIDER = Object.const_get(ENV['PRIMARY_PROVIDER'].capitalize)
SECONDARY_PROVIDER = Object.const_get(ENV['SECONDARY_PROVIDER'].capitalize)

# Base class for application
class ApplicationController < Sinatra::Base
  post '/email' do
    primary = PRIMARY_PROVIDER.new(params)

    # don't try to send if post params were not valid.
    halt 422, JSON.generate(primary.errors) unless primary.valid?

    # try to send with primary provider
    halt 200, primary.json_response if primary.schedule

    # try secondary provider if we haven't returned yet
    secondary = SECONDARY_PROVIDER.new(params)
    halt 200, secondary.json_response if secondary.schedule

    # both providers did not send return 500 and
    halt 500
  end

  get '/status' do
    status 200
    body 'Ok'
  end
end
